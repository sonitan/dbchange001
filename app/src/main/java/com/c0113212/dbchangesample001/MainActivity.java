package com.c0113212.dbchangesample001;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    String FILE_NAME="DBSetting";
    String FILE_NAME02="TableLayout";
    String DB_NAME=null;
    String TB_NAME=null;
    String K_NAME001=null;
    String K_NAME002=null;
    String K_NAME003=null;
    String Table_kou001=null;
    String Table_kou002=null;
    String Table_kou003=null;

    CreateProductHelper helper = null;
    SQLiteDatabase db01 = null;
    SQLiteDatabase db02 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button db_insert = (Button)findViewById(R.id.db_insert);
        db_insert.setTag("insert");
        db_insert.setOnClickListener(new DbButton());
        Button db_show = (Button)findViewById(R.id.bt_table);
        db_show.setTag("show");
        db_show.setOnClickListener(new DbButton());

        try{
            FileInputStream stream = openFileInput(FILE_NAME);
            BufferedReader in
                    =new BufferedReader(new InputStreamReader(stream));
            String line ="";
            while ((line=in.readLine())!=null){
                if(line.indexOf("DBNAME")!=-1){
                    DB_NAME = HtmlTagRemover1(line);
                    Log.d("ファイル内容:",DB_NAME);
                }
                if(line.indexOf("TBNAME")!=-1){
                    TB_NAME = HtmlTagRemover1(line);
                    Log.d("ファイル内容:",TB_NAME);
                }
                if(line.indexOf("Koumoku001")!=-1){
                    K_NAME001 = HtmlTagRemover1(line);
                    Log.d("ファイル内容:",K_NAME001);
                }
                if(line.indexOf("Koumoku002")!=-1){
                    K_NAME002 = HtmlTagRemover1(line);
                    Log.d("ファイル内容:",K_NAME002);
                }
                if(line.indexOf("Koumoku003")!=-1){
                    K_NAME003 = HtmlTagRemover1(line);
                    Log.d("ファイル内容:",K_NAME003);
                }
            }
            in.close();
            // DB作成
            helper = new CreateProductHelper(MainActivity.this,DB_NAME);
            Log.d("DB作成","しようとしてる？");
        }catch (Exception e){
            Log.d("ファイルないんとちゃう？",e.toString());
        }


        try {
            db01 = helper.getWritableDatabase();
            // SQL文定義
            //text not nullがテキストの登録intger not nullは数字の登録
            //テーブル名product 県（txt）市町村(txt) 救援物資(txt)が記述されている
            String sql
                    = "create table product (" +
                    "_id integer primary key autoincrement," +
                    "prefecture text not null," +
                    "city text not null," +
                    "help text not null);";
            // SQL実行
            db01.execSQL(sql);
            Log.d("DB登録チェック", "テーブル作成");
        } catch (Exception e) {
            Log.d("DB登録チェック", "(product)テーブル作成されてるらしいっすよ");
        }


        try {
            // 該当DBオブジェクト取得
            db02 = helper.getWritableDatabase();

            if(K_NAME001 !=null&&K_NAME002 !=null&&K_NAME003 !=null){
                Log.d("項目3","まであるね");

                //             SQL文定義
//            text not nullがテキストの登録intger not nullは数字の登録
//            テーブル名product 県（txt）市町村(txt) 救援物資(txt)が記述されている
                String sql02
                        = "create table "+TB_NAME+" (" +
                        "_id integer primary key autoincrement," +
                        K_NAME001+" text not null," +
                        K_NAME002+" text not null," +
                        K_NAME003+" text not null);";
                // SQL実行
                db02.execSQL(sql02);
                K_NAME001=null;K_NAME002=null;K_NAME003=null;
            }
            else if(K_NAME001 !=null&&K_NAME002 !=null){
                Log.d("項目2", "まであるね");
                String sql02
                        = "create table "+TB_NAME+" (" +
                        "_id integer primary key autoincrement," +
                        K_NAME001+" text not null," +
                        K_NAME002+" text not null);";
                // SQL実行
                db02.execSQL(sql02);
                K_NAME001=null;K_NAME002=null;K_NAME003=null;


            }
            else {
                Log.d("項目1","まであるね");
                String sql02
                        = "create table "+TB_NAME+" (" +
                        "_id integer primary key autoincrement," +
                        K_NAME001+" text not null);";
                // SQL実行
                db02.execSQL(sql02);
                K_NAME001=null;K_NAME002=null;K_NAME003=null;
            }

            Log.d("DB登録チェック", "テーブル作成");
        } catch (Exception e) {
            Log.d("DB登録チェック", TB_NAME+":テーブル作成されてるらしいっすよ");
        }
    }

    // DBの処理
    class DbButton implements View.OnClickListener {
        // onClickメソッド(ボタンクリック時イベントハンドラ)
        public void onClick(View v) {
            // タグの取得
            String tag = (String) v.getTag();



            // 登録ボタンが押された場合
            if (tag.equals("insert")) {
                EditText db_name=(EditText)findViewById(R.id.db_name);
                EditText tb_name=(EditText)findViewById(R.id.table_name);
                EditText db_koumoku001=(EditText)findViewById(R.id.koumoku_01);
                EditText db_koumoku002=(EditText)findViewById(R.id.koumoku_02);
                EditText db_koumoku003=(EditText)findViewById(R.id.koumoku_03);

                EditText tb_kou001=(EditText)findViewById(R.id.tb_koumoku_01);
                EditText tb_kou002=(EditText)findViewById(R.id.tb_koumoku_02);
                EditText tb_kou003=(EditText)findViewById(R.id.tb_koumoku_03);

                if(db_name.length() != 0&&tb_name.length() != 0&&db_koumoku001.length() != 0&&tb_kou001.length() !=0){
                    try {
                        //ファイルの内容削除
                        deleteFile(FILE_NAME);
                        //ファイルの内容削除
                        deleteFile(FILE_NAME02);
                        Log.d("ファイル処理", "DB登録開始");
                        String msg;

                        FileOutputStream stream
                                = openFileOutput(FILE_NAME, MODE_APPEND);
                        BufferedWriter out
                                = new BufferedWriter(new OutputStreamWriter(stream));

                        FileOutputStream stream02
                                = openFileOutput(FILE_NAME02, MODE_APPEND);
                        BufferedWriter out02
                                = new BufferedWriter(new OutputStreamWriter(stream02));

                        out.write("DBNAME:"+db_name.getText().toString());
                        out.newLine();
                        out.write("TBNAME:"+tb_name.getText().toString());
                        out.newLine();
                        out.write("Koumoku001:"+db_koumoku001.getText().toString());
                        out.newLine();
                        out02.write("TableLayout01:" + tb_kou001.getText().toString());
                        out02.newLine();

                        if(db_koumoku002.length() != 0){
                            out.write("Koumoku002:"+db_koumoku002.getText().toString());
                            out.newLine();
                            out02.write("TableLayout02:"+tb_kou002.getText().toString());
                            out02.newLine();
                        }
                        if(db_koumoku003.length() != 0){
                            out.write("Koumoku003:"+db_koumoku003.getText().toString());
                            out.newLine();
                            out02.write("TableLayout03:"+tb_kou003.getText().toString());
                            out02.newLine();
                        }

                        out.close();
                        out02.close();
                    }catch (Exception e){

                    }

                    try{
                        FileInputStream stream = openFileInput(FILE_NAME);
                        BufferedReader in
                                =new BufferedReader(new InputStreamReader(stream));
                        String line ="";
                        while ((line=in.readLine())!=null){
                            if(line.indexOf("DBNAME")!=-1){
                                DB_NAME = HtmlTagRemover1(line);
                                Log.d("ファイル内容:",DB_NAME);
                            }
                            if(line.indexOf("TBNAME")!=-1){
                                TB_NAME = HtmlTagRemover1(line);
                                Log.d("ファイル内容:",TB_NAME);
                            }
                            if(line.indexOf("Koumoku001")!=-1){
                                K_NAME001 = HtmlTagRemover1(line);
                                Log.d("ファイル内容:",K_NAME001);
                            }
                            if(line.indexOf("Koumoku002")!=-1){
                                K_NAME002 = HtmlTagRemover1(line);
                                Log.d("ファイル内容:",K_NAME002);
                            }
                            if(line.indexOf("Koumoku003")!=-1){
                                K_NAME003 = HtmlTagRemover1(line);
                                Log.d("ファイル内容:",K_NAME003);
                            }
                        }
                        in.close();
                        // DB作成
                        helper = new CreateProductHelper(MainActivity.this,DB_NAME);
                        Log.d("DB"+TB_NAME,":作成完了");
                    }catch (Exception e){
                        Log.d("DBFILEファイルないんとちゃう？",e.toString());
                    }



                    try {
                        db01 = helper.getWritableDatabase();
                        // SQL文定義
                        //text not nullがテキストの登録intger not nullは数字の登録
                        //テーブル名product 県（txt）市町村(txt) 救援物資(txt)が記述されている
                        String sql
                                = "create table product (" +
                                "_id integer primary key autoincrement," +
                                "prefecture text not null," +
                                "city text not null," +
                                "help text not null);";
                        // SQL実行
                        db01.execSQL(sql);
                        Log.d("DB登録チェック", "テーブル作成");
                    } catch (Exception e) {
                        Log.d("DB登録チェック", "(product)テーブル作成されてるらしいっすよ");
                    }


                    try {
                        // 該当DBオブジェクト取得
                        db02 = helper.getWritableDatabase();

                        if(K_NAME001 !=null&&K_NAME002 !=null&&K_NAME003 !=null){
                            Log.d("項目3","まであるね");

                            //             SQL文定義
//            text not nullがテキストの登録intger not nullは数字の登録
//            テーブル名product 県（txt）市町村(txt) 救援物資(txt)が記述されている
                            String sql02
                                    = "create table "+TB_NAME+" (" +
                                    "_id integer primary key autoincrement," +
                                    K_NAME001+" text not null," +
                                    K_NAME002+" text not null," +
                                    K_NAME003+" text not null);";
                            // SQL実行
                            db02.execSQL(sql02);
                            K_NAME001=null;K_NAME002=null;K_NAME003=null;

                            try{
                                FileInputStream stream02 = openFileInput(FILE_NAME02);
                                BufferedReader in02
                                        =new BufferedReader(new InputStreamReader(stream02));
                                String line ="";

                                while ((line=in02.readLine())!=null){
                                    if(line.indexOf("TableLayout03:")!=-1){
                                        Table_kou003 = HtmlTagRemover1(line);
                                        Log.d("ファイル内容(TableLayout):",Table_kou003);
                                    }
                                    if(line.indexOf("TableLayout02:")!=-1){
                                        Table_kou002 = HtmlTagRemover1(line);
                                        Log.d("ファイル内容(TableLayout):",Table_kou002);
                                    }
                                    if(line.indexOf("TableLayout01:")!=-1){
                                        Table_kou001 = HtmlTagRemover1(line);
                                        Log.d("ファイル内容(TableLayout):",Table_kou001);
                                    }
                                }
                                in02.close();
                                Log.d("TableLayout","３つ格納");
                            }catch (Exception e){
                                Log.d("ファイルないんとちゃう？",e.toString());
                            }
                        }


                        else if(K_NAME001 !=null&&K_NAME002 !=null){
                            Log.d("項目2", "まであるね");
                            String sql02
                                    = "create table "+TB_NAME+" (" +
                                    "_id integer primary key autoincrement," +
                                    K_NAME001+" text not null," +
                                    K_NAME002+" text not null);";
                            // SQL実行
                            db02.execSQL(sql02);
                            K_NAME001=null;K_NAME002=null;K_NAME003=null;

                            try{
                                FileInputStream stream = openFileInput(FILE_NAME02);
                                BufferedReader in
                                        =new BufferedReader(new InputStreamReader(stream));
                                String line ="";
                                while ((line=in.readLine())!=null){
                                    if(line.indexOf("TableLayout02:")!=-1){
                                        Table_kou002 = HtmlTagRemover1(line);
                                        Log.d("ファイル内容(TableLayout):",Table_kou002);
                                    }
                                    if(line.indexOf("TableLayout01:")!=-1){
                                        Table_kou001 = HtmlTagRemover1(line);
                                        Log.d("ファイル内容(TableLayout):",Table_kou001);
                                    }

                                }
                                in.close();
                                Log.d("TableLayout","３つ格納");
                            }catch (Exception e){
                                Log.d("ファイルないんとちゃう？",e.toString());
                            }
                        }
                        else {
                            Log.d("項目1","まであるね");
                            String sql02
                                    = "create table "+TB_NAME+" (" +
                                    "_id integer primary key autoincrement," +
                                    K_NAME001+" text not null);";
                            // SQL実行
                            db02.execSQL(sql02);
                            K_NAME001=null;K_NAME002=null;K_NAME003=null;
                            try{
                                FileInputStream stream = openFileInput(FILE_NAME02);
                                BufferedReader in
                                        =new BufferedReader(new InputStreamReader(stream));
                                String line ="";
                                while ((line=in.readLine())!=null){
                                    if(line.indexOf("TableLayout01:")!=-1){
                                        Table_kou001 = HtmlTagRemover1(line);
                                        Log.d("ファイル内容(TableLayout):",Table_kou001);
                                    }

                                }
                                in.close();
                                Log.d("TableLayout","３つ格納");
                            }catch (Exception e){
                                Log.d("ファイルないんとちゃう？",e.toString());
                            }
                        }

                        Log.d("DB登録チェック", "テーブル作成");
                    } catch (Exception e) {
                        Log.d("DB登録チェック", TB_NAME+":テーブル作成されてるらしいっすよ");
                    }

                }
                else{
                    Toast.makeText(MainActivity.this, "データベース名："+db_name.getText().toString() +"\nテーブル名："+tb_name.getText().toString() +"\n項目1："+db_koumoku001.getText().toString(), Toast.LENGTH_LONG).show();
                }

            }
            // 登録ボタンが押された場合
            if (tag.equals("show")) {
                // テーブルレイアウトオブジェクト取得
                TableLayout tablelayout = (TableLayout) findViewById(R.id.tb_list);
                //テーブルレイアウトを一度リセット
                tablelayout.removeAllViews();
                // テーブルレイアウトの表示範囲を設定
                tablelayout.setStretchAllColumns(true);

                // テーブル一覧のヘッダ部設定
                TableRow headrow = new TableRow(MainActivity.this);

                TextView headtxt1 = new TextView(MainActivity.this);
                headtxt1.setText("県名");
                headtxt1.setGravity(Gravity.CENTER_HORIZONTAL);
                headtxt1.setWidth(10);

                TextView headtxt2 = new TextView(MainActivity.this);
                headtxt2.setText("地域");
                headtxt2.setGravity(Gravity.CENTER_HORIZONTAL);
                headtxt2.setWidth(10);

                TextView headtxt3 = new TextView(MainActivity.this);
                headtxt3.setText("支援物資");
                headtxt3.setGravity(Gravity.CENTER_HORIZONTAL);
                headtxt3.setWidth(10);

                //addView
                headrow.addView(headtxt1);
                headrow.addView(headtxt2);
                headrow.addView(headtxt3);

                if(Table_kou001 !=null){
                    TextView headtxt4 = new TextView(MainActivity.this);
                    headtxt4.setText(Table_kou001);
                    headtxt4.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt4.setWidth(10);
                    headrow.addView(headtxt4);
                }
                if(Table_kou002!=null){
                    TextView headtxt5 = new TextView(MainActivity.this);
                    headtxt5.setText(Table_kou002);
                    headtxt5.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt5.setWidth(10);
                    headrow.addView(headtxt5);
                }
                if(Table_kou003!=null){
                    TextView headtxt6 = new TextView(MainActivity.this);
                    headtxt6.setText(Table_kou003);
                    headtxt6.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt6.setWidth(10);
                    headrow.addView(headtxt6);
                }

                //格納
                tablelayout.addView(headrow);

            }
        }
    }

    public static String HtmlTagRemover1(String str) {
        // 文字列のすべてのタグを取り除く
        str = str.replaceAll("DBNAME:", "");
        str = str.replaceAll("TBNAME:", "");
        str = str.replaceAll("Koumoku001:", "");
        str = str.replaceAll("Koumoku002:", "");
        return str.replaceAll("Koumoku003:", "");
    }

}
